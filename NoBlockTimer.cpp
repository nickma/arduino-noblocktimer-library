//
// Created by nickma on 29.10.18.
//

#include "NoBlockTimer.h"
#include <math.h>

/**
 * Four scenarios for ms and t->nextExecTime:
 * 1.) no overflow at all OR both overflow:
 * 1.1) ms < t->nextExecTime  -> false
 * 1.2) ms >= t->nextExecTime  -> true
 * 2.) one of them had an overflow (single_sided_overflow):
 * 2.1) ms overflow, nextExecTime not -> true
 * 2.2) ms not overflow, nextExecTime overflow -> false
 *
 * @param ms   arduino millis()
 * @param t    timer struct
 * @return whether or not the timer triggered
 */
bool timerCheck (const unsigned long ms, NoBlockTimer *t) {
    bool rc = false;
    const unsigned long ms_half_way = pow(2,32)-1/2;
    const unsigned long delta = (ms > t->nextExecTime) ? (ms - t->nextExecTime) : (t->nextExecTime - ms);

    const bool single_sided_overflow = delta > ms_half_way; //time deltas > ms_half_way not supported
    if (single_sided_overflow) {
        // Case 2.1 and 2.2
        const bool nextEx_overflow = t->nextExecTime < ms_half_way;
        rc = !nextEx_overflow;
    } else if (ms >= t->nextExecTime) {
        // Case 1.2
        rc = true;
    } // else Case 1.1 (as set per default)

    if (rc) {
        t->nextExecTime += t->timeout;
        // could not keep pace
        if (delta > 5 * t->timeout && !single_sided_overflow)
            t->nextExecTime = ms + t->timeout;
    }
    return rc;
}