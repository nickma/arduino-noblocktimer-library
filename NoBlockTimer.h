//
// Created by nickma on 29.10.18.
//

#ifndef LIEBHERR_FREEZER_NOBLOCKTIMER_H
#define LIEBHERR_FREEZER_NOBLOCKTIMER_H

struct NoBlockTimer {
    unsigned long       nextExecTime;  //equivalent with the lastRefreshTime
    const unsigned long timeout;
};

bool timerCheck(const unsigned long ms, NoBlockTimer *t);

#endif //LIEBHERR_FREEZER_NOBLOCKTIMER_H
