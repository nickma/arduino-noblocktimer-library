#include <iostream>
#include <limits>
#include "greatest.h"
#include "../NoBlockTimer.h"

using namespace std;

TEST tCheck_test() {
    const unsigned long maximum = std::numeric_limits<unsigned long>::max();
    NoBlockTimer timer1 = {0, 1000}; //Run every 1000ms
    ASSERT(timerCheck(0, &timer1));
    ASSERT(timerCheck(1000, &timer1));

    unsigned long ms;
    // 1.1
    ms = 5;
    timer1.nextExecTime = 42;
    ASSERT_FALSE(timerCheck(ms, &timer1));

    // 1.2
    ms = 42;
    timer1.nextExecTime = 5;
    ASSERT(timerCheck(ms, &timer1));
    ms = timer1.nextExecTime = 42;
    ASSERT(timerCheck(ms, &timer1));

    // 2.1
    ms = maximum + 42; //overflow
    timer1.nextExecTime = maximum; //no overflow
    ASSERT(timerCheck(ms, &timer1));
    ASSERT_EQ(maximum+timer1.timeout, timer1.nextExecTime);

    // 2.2 nextExecTime overflow
    ms = maximum;
    timer1.nextExecTime = maximum + timer1.timeout;
    ASSERT_FALSE(timerCheck(ms, &timer1));
    ASSERT_EQ(maximum + timer1.timeout, timer1.nextExecTime);

    PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char **argv)
{
    //update_facts(state);
    
    GREATEST_MAIN_BEGIN();
    RUN_TEST(tCheck_test);
    GREATEST_MAIN_END();        /* display results */
    return 0;
}