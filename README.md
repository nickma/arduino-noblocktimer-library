Note up front: This Project uses the ISC licensed testing framework "greatest.h".
For further information see https://github.com/silentbicycle/greatest


# NoBlockTimer

is meant to be used on top of the Arduino(R) millis() counter as a *non blocking*,
*overflow robust* and *overload robust* timer.
It is further meant to be called within the main loop() of every Arduino(R) program.


## Basic Usage
```c
NoBlockTimer timer1 = {0, 1000}; //Start at 0ms, run every 1000ms
void runner1() {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

void loop() {
  if (tCheck(millis(), &timer1))
    runner1();
}
```